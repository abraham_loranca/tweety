<div class="border border-blue-400 rounded-lg px-8 py-6 mb-8">

    <form method="POST" action="/tweets">
    @csrf
    <textarea  autofocus required placeholder="what's up doc?" name="body" id="body" class="w-full rounded">
        </textarea>

        <hr class="my-4">
        <footer class="flex justify-between items-center">
            <img class="rounded-full mr-2"  width="50" height="50" src="{{auth()->user()->avatar}}" alt="your avatar">
            <button class="bg-blue-500 rounded-lg hover:bg-blue-600 rounded-lg shadow py-2 px-10 text-sm text-white " type="submit ">
            Tweet-a-roo!
        </button>
        </footer>
    <form>
    @error('body')

    <p class="text-red-red-500 text-sm mt-2">{{$message}}</p>
    @enderror
</div>