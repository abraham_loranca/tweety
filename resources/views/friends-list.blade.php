<div class="bg-gray-200 border border-gray-400 rounded-lg py-4 px-2"> 
<h3 class="font-bold text-xl mb-4">Following</h3>
<ul>
    @forelse(auth()->user()->follows as $user)
    <li class=" {{$loop->last ? '' : 'mb-4'}}">
        <div >
          
           <a href="{{route('profile', $user)}}" class="flex items-center text-sm">
                   <img width="40" height="40" class="rounded-full mr-4"
             src="{{$user->avatar}}" 
             alt=""> 
             {{$user->name}}
                    </a>
            
        </div>
    </li>
    @empty
    <p class="py-4">No friends Yet.</p>
    @endforelse
</ul> 
</div>