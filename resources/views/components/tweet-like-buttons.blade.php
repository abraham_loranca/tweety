        <div class="flex mt-2">
            <form method="POST" action="/tweets/{{$tweet->id}}/like">
                @csrf
                <div class="flex items-center  {{ $tweet->isLikedBy(current_user()) ? 'text-blue-500' : 'text-gray-500' }}">
                    <svg viewBox="0 0 20 20" class=" mr-1 w-3">
            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
              <g class="fill-current">
            
                <path d="M10 20a10 10 0 1 1 0-20 10 10 0 0 1 0 20zm0-2a8 8 0 1 0 0-16 8 8 0 0 0 0 16zM6.5 9a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm7 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm2.16 3a6 6 0 0 1-11.32 0h11.32z"></path>
              </g>
              </g>
              </svg>
                    <button type="submit" class="text-xs text-gray-500">
                {{$tweet->likes ?: 0}}
                  </button>
                </div>
            </form>
            <form method="POST" action="/tweets/{{$tweet->id}}/like">
                @csrf @method('DELETE')
                <div class="flex items-center {{ $tweet->isDislikedBy(current_user()) ? 'text-blue-500' : 'text-gray-500' }}">
                    <svg viewBox="0 0 20 20" class="ml-2 mr-1 w-3">
                  <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g class="fill-current">
                  
                     <path d="M10 20a10 10 0 1 1 0-20 10 10 0 0 1 0 20zm0-2a8 8 0 1 0 0-16 8 8 0 0 0 0 16zM6.5 9a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm7 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm2.16 6H4.34a6 6 0 0 1 11.32 0z"></path>
                    </g>
                    </g>
            </svg>
                    <button type="submit" class="text-xs text-gray-500">
                  {{$tweet->dislikes ?: 0}}
                    </button>
                </div>

            </form>


        </div>