<x-app>
    <div>
        @foreach($users as $user)
        <a href="{{$user->path()}}" class="flex items-center mb-6">

            <img src="{{$user->avatar}}" alt="{{$user->name}}'s avatar" width="60" class="rounded mr-4">

            <div>
                <h4 class="font-bold">{{'@'.$user->username}}</h4>
            </div>

        </a> @endforeach
    </div>

</x-app>