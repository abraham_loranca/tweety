<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use app\Followable;
use app\User;
class FollowsController extends Controller
{
    public function store(User $user)
    {
      
        auth()->user()->toggleFollow($user);
        return back();
        
    }
}
