-- --------------------------------------------------------
-- Host:                         localhost
-- Versión del servidor:         10.4.14-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para tweety
CREATE DATABASE IF NOT EXISTS `tweety` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci */;
USE `tweety`;

-- Volcando estructura para tabla tweety.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla tweety.failed_jobs: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Volcando estructura para tabla tweety.follows
CREATE TABLE IF NOT EXISTS `follows` (
  `user_id` bigint(20) unsigned NOT NULL,
  `following_user_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`following_user_id`),
  KEY `follows_following_user_id_foreign` (`following_user_id`),
  CONSTRAINT `follows_following_user_id_foreign` FOREIGN KEY (`following_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `follows_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla tweety.follows: ~9 rows (aproximadamente)
/*!40000 ALTER TABLE `follows` DISABLE KEYS */;
INSERT IGNORE INTO `follows` (`user_id`, `following_user_id`, `created_at`, `updated_at`) VALUES
	(1, 2, NULL, NULL),
	(1, 3, NULL, NULL),
	(1, 6, NULL, NULL),
	(1, 8, NULL, NULL),
	(1, 9, NULL, NULL),
	(1, 13, NULL, NULL),
	(2, 1, NULL, NULL),
	(2, 3, NULL, NULL),
	(2, 5, NULL, NULL),
	(13, 1, NULL, NULL);
/*!40000 ALTER TABLE `follows` ENABLE KEYS */;

-- Volcando estructura para tabla tweety.likes
CREATE TABLE IF NOT EXISTS `likes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `tweet_id` bigint(20) unsigned NOT NULL,
  `liked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `likes_user_id_foreign` (`user_id`),
  KEY `likes_tweet_id_foreign` (`tweet_id`),
  CONSTRAINT `likes_tweet_id_foreign` FOREIGN KEY (`tweet_id`) REFERENCES `tweets` (`id`) ON DELETE CASCADE,
  CONSTRAINT `likes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla tweety.likes: ~17 rows (aproximadamente)
/*!40000 ALTER TABLE `likes` DISABLE KEYS */;
INSERT IGNORE INTO `likes` (`id`, `user_id`, `tweet_id`, `liked`, `created_at`, `updated_at`) VALUES
	(1, 1, 2, 1, '2021-02-26 20:39:30', '2021-02-26 20:39:32'),
	(2, 1, 3, 0, '2021-02-26 20:39:30', '2021-02-26 20:39:32'),
	(3, 1, 104, 1, '2021-02-27 05:34:08', '2021-02-27 05:46:11'),
	(4, 1, 105, 0, '2021-02-27 05:34:13', '2021-02-27 05:34:13'),
	(5, 1, 112, 1, '2021-02-27 05:34:23', '2021-02-27 16:31:25'),
	(6, 1, 111, 1, '2021-02-27 05:38:23', '2021-02-27 05:38:31'),
	(7, 1, 110, 0, '2021-02-27 05:39:08', '2021-02-27 05:39:11'),
	(8, 1, 22, 1, '2021-02-27 05:50:03', '2021-02-27 05:50:03'),
	(9, 1, 84, 0, '2021-02-27 05:50:38', '2021-02-27 05:50:38'),
	(10, 1, 100, 1, '2021-02-27 16:28:02', '2021-02-27 16:28:02'),
	(11, 13, 22, 1, '2021-02-27 16:29:59', '2021-02-27 16:30:45'),
	(12, 13, 30, 1, '2021-02-27 16:30:39', '2021-02-27 16:30:39'),
	(13, 13, 35, 1, '2021-02-27 16:30:54', '2021-02-27 16:30:54'),
	(14, 1, 75, 1, '2021-02-27 16:31:28', '2021-02-27 16:31:28'),
	(15, 1, 76, 1, '2021-02-27 16:31:29', '2021-02-27 16:31:29'),
	(16, 1, 69, 0, '2021-02-27 16:32:20', '2021-02-27 16:32:20'),
	(17, 1, 99, 0, '2021-02-27 16:32:21', '2021-02-27 16:32:21');
/*!40000 ALTER TABLE `likes` ENABLE KEYS */;

-- Volcando estructura para tabla tweety.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla tweety.migrations: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT IGNORE INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_08_19_000000_create_failed_jobs_table', 1),
	(4, '2021_02_13_055435_create_tweets_table', 1),
	(5, '2021_02_13_172405_create_follows_table', 1),
	(6, '2021_02_20_155238_create_likes_tables', 2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Volcando estructura para tabla tweety.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla tweety.password_resets: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Volcando estructura para tabla tweety.tweets
CREATE TABLE IF NOT EXISTS `tweets` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `body` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla tweety.tweets: ~111 rows (aproximadamente)
/*!40000 ALTER TABLE `tweets` DISABLE KEYS */;
INSERT IGNORE INTO `tweets` (`id`, `user_id`, `body`, `created_at`, `updated_at`) VALUES
	(2, 1, 'mi primer tweet', '2021-02-18 18:44:05', '2021-02-18 18:44:05'),
	(3, 1, 'Voluptatem in debitis porro inventore velit aut.', '2021-02-19 03:35:36', '2021-02-19 03:35:36'),
	(4, 1, 'Pariatur eum ut nostrum aspernatur.', '2021-02-19 03:35:36', '2021-02-19 03:35:36'),
	(5, 1, 'Perspiciatis nisi accusamus expedita voluptate sit autem voluptate dolores.', '2021-02-19 03:35:36', '2021-02-19 03:35:36'),
	(6, 1, 'Qui quae dolores ut deserunt et fugiat qui.', '2021-02-19 03:35:36', '2021-02-19 03:35:36'),
	(7, 1, 'Eligendi quaerat id doloremque fugiat asperiores non.', '2021-02-19 03:35:36', '2021-02-19 03:35:36'),
	(8, 1, 'Tempora et autem qui.', '2021-02-19 03:35:36', '2021-02-19 03:35:36'),
	(9, 1, 'Ipsum nostrum porro sequi consequatur aut quia dignissimos quia.', '2021-02-19 03:35:36', '2021-02-19 03:35:36'),
	(10, 1, 'Tempora sequi cum ipsa porro deserunt dolor.', '2021-02-19 03:35:36', '2021-02-19 03:35:36'),
	(11, 1, 'Qui quis necessitatibus consequatur similique ullam ut molestiae.', '2021-02-19 03:35:36', '2021-02-19 03:35:36'),
	(12, 1, 'Quo quam dolor et et qui.', '2021-02-19 03:35:36', '2021-02-19 03:35:36'),
	(13, 1, 'Sit perspiciatis molestiae eligendi blanditiis et enim.', '2021-02-19 03:35:36', '2021-02-19 03:35:36'),
	(14, 1, 'Tempora pariatur soluta et.', '2021-02-19 03:35:36', '2021-02-19 03:35:36'),
	(15, 1, 'Iusto ut id incidunt.', '2021-02-19 03:35:36', '2021-02-19 03:35:36'),
	(16, 1, 'Velit veniam qui voluptate non vero nihil ratione.', '2021-02-19 03:35:36', '2021-02-19 03:35:36'),
	(17, 1, 'Molestias at sed corrupti aut velit sit.', '2021-02-19 03:35:36', '2021-02-19 03:35:36'),
	(18, 1, 'Dolorum earum rerum minus qui.', '2021-02-19 03:35:36', '2021-02-19 03:35:36'),
	(19, 1, 'Ut velit qui laudantium voluptatibus consequatur laboriosam illo.', '2021-02-19 03:35:36', '2021-02-19 03:35:36'),
	(20, 1, 'Animi dolor unde autem ducimus.', '2021-02-19 03:35:36', '2021-02-19 03:35:36'),
	(21, 1, 'Temporibus sequi cum quidem.', '2021-02-19 03:35:36', '2021-02-19 03:35:36'),
	(22, 1, 'Qui dolorum laboriosam praesentium occaecati illum est.', '2021-02-19 03:35:37', '2021-02-19 03:35:37'),
	(23, 1, 'Eveniet ratione maiores dicta saepe est est.', '2021-02-19 03:35:37', '2021-02-19 03:35:37'),
	(24, 1, 'Omnis nostrum nobis libero blanditiis.', '2021-02-19 03:35:37', '2021-02-19 03:35:37'),
	(25, 1, 'Adipisci omnis autem blanditiis nihil.', '2021-02-19 03:35:37', '2021-02-19 03:35:37'),
	(26, 1, 'Qui id sed et quas eius exercitationem accusamus cumque.', '2021-02-19 03:35:37', '2021-02-19 03:35:37'),
	(27, 1, 'Ipsa laboriosam nemo est ipsa maxime sapiente pariatur.', '2021-02-19 03:35:37', '2021-02-19 03:35:37'),
	(28, 1, 'Ut aspernatur ducimus quisquam excepturi reprehenderit nostrum aut sequi.', '2021-02-19 03:35:37', '2021-02-19 03:35:37'),
	(29, 1, 'Inventore itaque expedita reiciendis tempore.', '2021-02-19 03:35:37', '2021-02-19 03:35:37'),
	(30, 1, 'Voluptatem et ut rem sint voluptatibus nisi.', '2021-02-19 03:35:37', '2021-02-19 03:35:37'),
	(31, 1, 'Nihil qui et voluptatem quo.', '2021-02-19 03:35:37', '2021-02-19 03:35:37'),
	(32, 1, 'Nobis aut quis voluptas quis nemo iste.', '2021-02-19 03:35:37', '2021-02-19 03:35:37'),
	(33, 1, 'Cumque optio ut aut perferendis sit et explicabo.', '2021-02-19 03:35:37', '2021-02-19 03:35:37'),
	(34, 1, 'Reprehenderit ducimus ut omnis id sed ducimus.', '2021-02-19 03:35:37', '2021-02-19 03:35:37'),
	(35, 1, 'Ea nisi unde dolore optio ratione ut quos.', '2021-02-19 03:35:37', '2021-02-19 03:35:37'),
	(36, 2, 'Eveniet adipisci dolorem illum eos culpa.', '2021-02-19 03:35:37', '2021-02-19 03:35:37'),
	(37, 2, 'Sed explicabo quod velit omnis velit repellendus.', '2021-02-19 03:35:37', '2021-02-19 03:35:37'),
	(38, 2, 'Assumenda voluptas repellendus et.', '2021-02-19 03:35:37', '2021-02-19 03:35:37'),
	(39, 2, 'Animi non fugiat non magnam velit.', '2021-02-19 03:35:37', '2021-02-19 03:35:37'),
	(40, 2, 'Dignissimos quaerat fugit non earum soluta modi eos.', '2021-02-19 03:35:37', '2021-02-19 03:35:37'),
	(41, 3, 'Natus quisquam totam expedita error numquam quos in.', '2021-02-19 03:35:37', '2021-02-19 03:35:37'),
	(42, 3, 'Doloremque unde vel accusamus magni unde maxime commodi.', '2021-02-19 03:35:37', '2021-02-19 03:35:37'),
	(43, 3, 'Autem est totam quisquam consequatur inventore voluptatum.', '2021-02-19 03:35:37', '2021-02-19 03:35:37'),
	(44, 3, 'Maxime sed praesentium sint et ipsam recusandae dolores.', '2021-02-19 03:35:37', '2021-02-19 03:35:37'),
	(45, 3, 'Dolores dolore omnis et molestiae quo.', '2021-02-19 03:35:37', '2021-02-19 03:35:37'),
	(46, 3, 'Aut tempora sed harum suscipit minima nisi.', '2021-02-19 03:35:37', '2021-02-19 03:35:37'),
	(47, 3, 'Sunt odit corrupti distinctio voluptatem.', '2021-02-19 03:35:38', '2021-02-19 03:35:38'),
	(48, 3, 'Fugiat rem rerum eveniet natus qui.', '2021-02-19 03:35:38', '2021-02-19 03:35:38'),
	(49, 4, 'Delectus enim vitae inventore sint aut.', '2021-02-19 03:35:38', '2021-02-19 03:35:38'),
	(50, 4, 'Officiis animi animi ratione omnis porro eos rerum.', '2021-02-19 03:35:38', '2021-02-19 03:35:38'),
	(51, 4, 'Vel est sequi assumenda ipsa.', '2021-02-19 03:35:38', '2021-02-19 03:35:38'),
	(52, 4, 'Atque qui suscipit similique corporis architecto omnis placeat consequatur.', '2021-02-19 03:35:38', '2021-02-19 03:35:38'),
	(53, 4, 'Dolorem quas harum quibusdam sed est voluptatem.', '2021-02-19 03:35:38', '2021-02-19 03:35:38'),
	(54, 4, 'Labore consequuntur blanditiis ipsam voluptatem.', '2021-02-19 03:35:38', '2021-02-19 03:35:38'),
	(55, 5, 'Non quisquam sit minima et hic ex.', '2021-02-19 03:35:38', '2021-02-19 03:35:38'),
	(56, 5, 'Veniam praesentium inventore quasi expedita.', '2021-02-19 03:35:38', '2021-02-19 03:35:38'),
	(57, 5, 'Voluptate nihil autem id nostrum.', '2021-02-19 03:35:38', '2021-02-19 03:35:38'),
	(58, 5, 'Iste odio corrupti esse quia aperiam saepe.', '2021-02-19 03:35:38', '2021-02-19 03:35:38'),
	(59, 5, 'Nostrum nobis necessitatibus rerum quia.', '2021-02-19 03:35:38', '2021-02-19 03:35:38'),
	(60, 6, 'Minima adipisci quo alias.', '2021-02-19 03:35:38', '2021-02-19 03:35:38'),
	(61, 6, 'Pariatur placeat unde autem aperiam quia.', '2021-02-19 03:35:38', '2021-02-19 03:35:38'),
	(62, 6, 'Et et ut sit id.', '2021-02-19 03:35:38', '2021-02-19 03:35:38'),
	(63, 6, 'Molestias sit repellat explicabo debitis perspiciatis velit pariatur at.', '2021-02-19 03:35:38', '2021-02-19 03:35:38'),
	(64, 7, 'Omnis nobis quis illo non et ut nulla alias.', '2021-02-19 03:35:38', '2021-02-19 03:35:38'),
	(65, 7, 'Quibusdam doloribus iste sed omnis et.', '2021-02-19 03:35:38', '2021-02-19 03:35:38'),
	(66, 8, 'Ratione id libero nam quo praesentium.', '2021-02-19 03:35:38', '2021-02-19 03:35:38'),
	(67, 8, 'Excepturi quia omnis cum possimus rerum a magni.', '2021-02-19 03:35:38', '2021-02-19 03:35:38'),
	(68, 9, 'Est a quia itaque et reiciendis et eligendi.', '2021-02-19 03:35:38', '2021-02-19 03:35:38'),
	(69, 9, 'Cupiditate molestiae iure voluptas nihil voluptates.', '2021-02-19 03:35:39', '2021-02-19 03:35:39'),
	(70, 10, 'Laudantium qui explicabo omnis et sunt et accusantium.', '2021-02-19 03:35:39', '2021-02-19 03:35:39'),
	(71, 10, 'Ut animi repudiandae qui molestiae.', '2021-02-19 03:35:39', '2021-02-19 03:35:39'),
	(72, 11, 'Maiores dolorem nihil hic rem.', '2021-02-19 03:35:39', '2021-02-19 03:35:39'),
	(73, 11, 'Qui explicabo exercitationem ullam veniam voluptate laborum.', '2021-02-19 03:35:39', '2021-02-19 03:35:39'),
	(74, 12, 'Nam vel et molestias ipsa consequuntur doloremque.', '2021-02-19 03:35:39', '2021-02-19 03:35:39'),
	(75, 2, 'Sit nulla molestiae occaecati a error labore error voluptatem.', '2021-02-19 03:35:39', '2021-02-19 03:35:39'),
	(76, 2, 'Fugiat placeat velit porro quidem repudiandae alias consequatur quia.', '2021-02-19 03:35:39', '2021-02-19 03:35:39'),
	(77, 3, 'Id qui nemo error non commodi.', '2021-02-19 03:35:39', '2021-02-19 03:35:39'),
	(78, 3, 'Itaque incidunt ea magni eligendi natus eos rerum nostrum.', '2021-02-19 03:35:39', '2021-02-19 03:35:39'),
	(79, 3, 'Sint ducimus consequatur officiis velit.', '2021-02-19 03:35:39', '2021-02-19 03:35:39'),
	(80, 4, 'Quam similique qui omnis occaecati facere unde.', '2021-02-19 03:35:39', '2021-02-19 03:35:39'),
	(81, 4, 'Vel iusto vero facere quos excepturi magnam praesentium.', '2021-02-19 03:35:39', '2021-02-19 03:35:39'),
	(82, 5, 'Soluta quod unde cumque voluptas culpa.', '2021-02-19 03:35:39', '2021-02-19 03:35:39'),
	(83, 5, 'Magnam eveniet ea autem sit.', '2021-02-19 03:35:39', '2021-02-19 03:35:39'),
	(84, 6, 'Dolor deserunt recusandae et deserunt maxime.', '2021-02-19 03:35:39', '2021-02-19 03:35:39'),
	(85, 6, 'Et in praesentium eos accusamus nobis nihil.', '2021-02-19 03:35:39', '2021-02-19 03:35:39'),
	(86, 7, 'Sed enim possimus sit rerum.', '2021-02-19 03:35:39', '2021-02-19 03:35:39'),
	(87, 7, 'Cumque voluptas voluptatem esse omnis nesciunt.', '2021-02-19 03:35:39', '2021-02-19 03:35:39'),
	(88, 8, 'In vitae delectus et sed.', '2021-02-19 03:35:39', '2021-02-19 03:35:39'),
	(89, 8, 'Enim dignissimos quae sed qui et vel fugit.', '2021-02-19 03:35:39', '2021-02-19 03:35:39'),
	(90, 9, 'Possimus distinctio nemo dignissimos molestiae dolor.', '2021-02-19 03:35:39', '2021-02-19 03:35:39'),
	(91, 9, 'Vero excepturi unde cupiditate voluptas.', '2021-02-19 03:35:39', '2021-02-19 03:35:39'),
	(92, 10, 'Natus voluptatem perferendis odio id et.', '2021-02-19 03:35:39', '2021-02-19 03:35:39'),
	(93, 10, 'Minima rerum eos est quia velit ea.', '2021-02-19 03:35:40', '2021-02-19 03:35:40'),
	(94, 11, 'Cumque officiis deleniti explicabo atque.', '2021-02-19 03:35:40', '2021-02-19 03:35:40'),
	(95, 11, 'Voluptas a neque et nemo.', '2021-02-19 03:35:40', '2021-02-19 03:35:40'),
	(96, 12, 'Voluptas maxime aut aut ut laudantium nam.', '2021-02-19 03:35:40', '2021-02-19 03:35:40'),
	(97, 12, 'Nihil alias quam id sunt nulla ratione in.', '2021-02-19 03:35:40', '2021-02-19 03:35:40'),
	(98, 9, 'Occaecati tenetur nulla aut quo officiis.', '2021-02-19 03:35:40', '2021-02-19 03:35:40'),
	(99, 9, 'Earum animi ut quibusdam deleniti adipisci dignissimos.', '2021-02-19 03:35:40', '2021-02-19 03:35:40'),
	(100, 8, 'Ipsam est et error inventore et.', '2021-02-19 03:35:40', '2021-02-19 03:35:40'),
	(101, 8, 'Repudiandae aut labore reiciendis dolorum laboriosam autem.', '2021-02-19 03:35:40', '2021-02-19 03:35:40'),
	(102, 7, 'Ipsam dolore harum voluptatum aut ab quia molestias accusamus.', '2021-02-19 03:35:40', '2021-02-19 03:35:40'),
	(103, 7, 'Perferendis voluptatem neque nesciunt laudantium.', '2021-02-19 03:35:40', '2021-02-19 03:35:40'),
	(104, 6, 'Laudantium dolorem aut tenetur pariatur.', '2021-02-19 03:35:40', '2021-02-19 03:35:40'),
	(105, 6, 'Provident vel et qui nam nostrum quod.', '2021-02-19 03:35:40', '2021-02-19 03:35:40'),
	(106, 5, 'Ut nostrum recusandae a.', '2021-02-19 03:35:40', '2021-02-19 03:35:40'),
	(107, 5, 'Aut est ea adipisci ipsa repudiandae quia.', '2021-02-19 03:35:40', '2021-02-19 03:35:40'),
	(108, 4, 'Reiciendis illum qui cupiditate et autem consequuntur quis.', '2021-02-19 03:35:40', '2021-02-19 03:35:40'),
	(109, 4, 'Aut nihil dolor dolor vel qui.', '2021-02-19 03:35:40', '2021-02-19 03:35:40'),
	(110, 3, 'Expedita ea qui quis sunt voluptatum.', '2021-02-19 03:35:40', '2021-02-19 03:35:40'),
	(111, 3, 'Sunt distinctio quae voluptatibus incidunt.', '2021-02-19 03:35:40', '2021-02-19 03:35:40'),
	(112, 2, 'Cum dolores non odit assumenda est magni molestiae vel.', '2021-02-19 03:35:40', '2021-02-19 03:35:40'),
	(113, 13, 'mi primer tweet', '2021-02-27 16:29:52', '2021-02-27 16:29:52');
/*!40000 ALTER TABLE `tweets` ENABLE KEYS */;

-- Volcando estructura para tabla tweety.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla tweety.users: ~12 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT IGNORE INTO `users` (`id`, `username`, `name`, `avatar`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'AbrahamR', 'abraham Rosas', 'avatars/2KCRIvbnUhmfjavbIChCrR6Ox4loxfLYv0whPHSh.jpg', 'abrahamloranca@gmail.com', NULL, '$2y$10$KRoefogBScUPN76SO2nmMuWcm38JhIl6vzOJeE6wUPqPcfVt2WV0K', NULL, '2021-02-18 18:10:48', '2021-02-18 22:40:35'),
	(2, 'LorancaR', 'Loranca rosas', 'avatars/1wpoCcGWgvbA6kHdbLBduq9GtOHSMZ97BgV5ZWNw.jpg', 'loranca@gmail.com', NULL, '$2y$10$Q20BteQJsWR85xJhLEPZB.eUxrZq84jjNjYrciOBG/UlUav3G0qtS', NULL, '2021-02-19 02:36:34', '2021-02-19 02:43:07'),
	(3, 'mclaughlin.suzanne', 'Naomi Gulgowski', NULL, 'pnikolaus@example.com', '2021-02-19 03:07:41', '$2y$10$tuzgP0xxZe5GEz8Ky0kRIeqfP8HZtSA9WoVlskIi2VJM4ulzVXLYq', 'xgwaMOOZQY', '2021-02-19 03:07:41', '2021-02-19 03:07:41'),
	(4, 'ljohns', 'Ardella Muller', NULL, 'lnienow@example.org', '2021-02-19 03:07:41', '$2y$10$ab3T6EJ36HtS1UiyswpdDu//e.lNkg8kmUFPTgDodK8HfsXlLdVy2', 'KbIvU3OdFv', '2021-02-19 03:07:41', '2021-02-19 03:07:41'),
	(5, 'legros.jaiden', 'Prof. Sedrick Ankunding Jr.', NULL, 'rutherford.edward@example.org', '2021-02-19 03:07:41', '$2y$10$ed2Erut.H4qo3AnSaT9Nyua0DcEuqTrj9lvQVIr1qpxHIIRiVoh3W', 'MWDmAsn5kg', '2021-02-19 03:07:42', '2021-02-19 03:07:42'),
	(6, 'xcormier', 'Lionel Green', NULL, 'wolf.selmer@example.net', '2021-02-19 03:07:41', '$2y$10$xAyq3IYUnuKRu.HR/vq2Pu0yZD/yIyB.mI5U/.EZKAzjlMTQTjco.', 'FBjM7KSyR8', '2021-02-19 03:07:42', '2021-02-19 03:07:42'),
	(7, 'porter.oreilly', 'Moises Mills III', NULL, 'lyda.lebsack@example.com', '2021-02-19 03:07:41', '$2y$10$EyyYgsR1XUYWVMDe2lF7neN4JVkEwDCAtm/Q6NXQTe44.YByP3LhK', '4mxELrgClE', '2021-02-19 03:07:42', '2021-02-19 03:07:42'),
	(8, 'qschmitt', 'Sim Lowe', NULL, 'valentina91@example.org', '2021-02-19 03:07:41', '$2y$10$ESSTTAHq/xRgoqLiINb2Dep4CipgMaEL9z6F6Qhcas0y/fKmplcoe', 'mmNWwiYtpw', '2021-02-19 03:07:42', '2021-02-19 03:07:42'),
	(9, 'rcartwright', 'Prof. Holly Runolfsson II', NULL, 'josefina.wehner@example.org', '2021-02-19 03:07:41', '$2y$10$pDznZrgHLov.tD7EMYNNh.238CTWxuw6HtOkbtZ1hOBDrATBLZHri', '6Pwz3zC648', '2021-02-19 03:07:42', '2021-02-19 03:07:42'),
	(10, 'twillms', 'Carleton Gutmann', NULL, 'vwalker@example.com', '2021-02-19 03:07:41', '$2y$10$wG2H4wcGDitgP3tuykb7auIVPCEl3zs8yf4CDyyN/lN.wlxqkkOBq', '9gi11XkTgm', '2021-02-19 03:07:42', '2021-02-19 03:07:42'),
	(11, 'jzulauf', 'Petra Mraz', NULL, 'dandre.barton@example.org', '2021-02-19 03:07:41', '$2y$10$v11LfW0MNGn0A7SElBPrueyOxCubIEUpjUvl1CzpvGhPnljSPIwfW', '8zVN5KklU7', '2021-02-19 03:07:42', '2021-02-19 03:07:42'),
	(12, 'dsenger', 'London Mills', NULL, 'mckenna.smith@example.net', '2021-02-19 03:07:41', '$2y$10$LJ4IqUTUHB9NU/UpgF9KgetM5HWCx/JermcDCVrLrb/FukSmM4uzK', 'oLeudZysoh', '2021-02-19 03:07:42', '2021-02-19 03:07:42'),
	(13, 'Lorsep', 'Loranca', 'avatars/t3tpA1TcQiB3xTSLfRaxUrNP9iUKHeafFz8Z8RH4.jpg', 'loraros6070@gmail.com', NULL, '$2y$10$7i8JAsSQRWmeb5PvIwdPEuK0THK5BlN1KKHG/R465OEMcIQPsyWdm', NULL, '2021-02-27 16:29:43', '2021-02-27 16:30:24');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
